# README

## Versions
* Ruby version - ruby 2.6.0
* Rails version - Rails 5.2.3

## Setting Up Rails Project
If you are following the guide and is already familiar with the basic steps to be done to reproduce your own repository, you can skip this part. 
1. Create and setup new rails project locally
    ```
    rails new sample-gitlab-ci-project -T          
    cd sample-gitlab-ci-project
    bundle install 
    ```

The `-T` option creates a new rails project without the defaul unit tests since we are going to use rspec.

2. Setup your remote repository hosted by https://gitlab.com and push your local rails project
    ```
    git remote add origin <REMOTE-REPOSITORY-URL>
    git add -A
    git commit -m "Initialize rails project"
    git push origin master
    ```
3. You can add a css framework like [semanticui](https://github.com/doabit/semantic-ui-sass) to provide a decent styling for the project.
4. Add necessary gems for rspec in your Gemfile.

    ```
    # Gemfile
    group :development, :test do
        # Call 'byebug' anywhere in the code to stop execution and get a debugger console
        gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
        %w[rspec-core rspec-expectations rspec-mocks rspec-rails rspec-support].each do |lib|
            gem lib, git: "https://github.com/rspec/#{lib}.git", branch: 'master'
        end
    end
    ```

    ```
    # Terminal
    bundle install
    rails generate rspec:install
    ```
5. Add other helpful gems to be used in this sample project.

    ```
    gem 'slim-rails'
    gem 'jquery-rails'
    gem 'rubocop', '~> 0.67.2', require: false
    gem 'rubocop-performance'
    gem 'fuubar'
    ```

    Note: Some of these gems have additional setup needed in order for them to work. Check their github pages for complete installation instructions of each gem.
6. Initialize a `static_pages_controller` and create a home template for a sample homepage. In order to replicate the scenario that I encountered with my setup from another project, we will add a jquery functionality that dynamically adds form field in our home page.

    ```
    rails generate controller static_pages home
    ```
7. Set your home page as the `root_path` in your `routes.rb`.

    ```
    # config/routes.rb
    root 'static_pages#home'
    ```
8. Modify the template `home.html.slim` for our homepage.
    ```
    # app/views/static_pages/home.html.slim
    .ui.fluid.container
    .ui.grid.stackable
        .ui.center.aligned.sixteen.wide.column
        h1 Welcome to Gitlab CI/CD Setup Guide
        .ui.divider
        .ui.eight.wide.column
        .ui.form
            .field
            #createNewTextbox
                input#add_new_field type='button' value='Add New Field' class='ui basic blue button'
            .field
            input name="mytext[]" type="text" /
            #container
    ```
9. In order to make the `Add New Field` button to work and add new `text_field` on click, add this jquery code to `application.js`.
    ```
    # application.js
    document.addEventListener('turbolinks:load', function() {
        document.getElementById('add_new_field').addEventListener("click", function() {
            var div = document.createElement("div");
            div.classList.add("field");

            
            var email_field = document.createElement("input");
            email_field.setAttribute("type", "text");
            email_field.setAttribute("name", "mytext[]");

            div.appendChild(email_field);

            document.getElementById("container").appendChild(div);
        });
    });    
    ```
10. If I haven't missed anything and if you properly set up your project, you can now start your rails server locally by running `rails server`.