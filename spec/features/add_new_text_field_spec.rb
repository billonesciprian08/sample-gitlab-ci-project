# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'AddNewTextFields', type: :feature, js: true do
  before(:each) do
    @text_field_count = all("input[name='mytext[]']")
  end

  scenario 'user clicks \'Add New Field\' button' do
    visit root_path
    page.evaluate_script("$('#add_new_field').trigger('click')")
    expect(all("input[name='mytext[]']")).not_to eq(@text_field_count)
  end
end
