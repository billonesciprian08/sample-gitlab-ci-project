// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require jquery
//= require activestorage
//= require turbolinks
//= require semantic-ui
//= require_tree .

document.addEventListener('turbolinks:load', function() {
  document.getElementById('add_new_field').addEventListener("click", function() {
    var div = document.createElement("div");
      div.classList.add("field");

    
    var email_field = document.createElement("input");
      email_field.setAttribute("type", "text");
      email_field.setAttribute("name", "mytext[]");

    div.appendChild(email_field);

    document.getElementById("container").appendChild(div);
  });
});
